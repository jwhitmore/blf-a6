Changes are:

- 1 sec pause between config blinks
- No strobes
- Only hidden modes are Turbo and Battery Check
- Mode 1:
    - Hidden Modes (Turbo & Batter Check)
    - Moon
    - Low
    - Medium
    - High
    - Turbo
- Mode 2:
    - Hidden Modes (Turbo & Batter Check)
    - Low
    - Medium
    - High

Modified from:

https://code.launchpad.net/~toykeeper/flashlight-firmware/trunk

Hoop's guide:

http://budgetlightforum.com/node/36216

Comfychair's guide:

http://budgetlightforum.com/node/29081

The command: avrdude -p t13 -c usbasp -n is a test command and will let you know if the chip is wired correctly. 

If you get the “initialization failed” message, you aren’t getting a good connection.

The command: avrdude -p t13 -c usbasp -u -e will erase the contents of the chip so that it can be re-written to.

Now that the chip is empty it is ready to be flashed with fresh firmware. Change the “current directory” by executing cd C:\avrusb so that AVRdude can find the file.

Finally, flash the driver with the new firmware: (change the star.hex part to whatever you named your .hex file)

Test Command:

If you get the “initialization failed” message, you aren’t getting a good connection.

avrdude -p t13 -c usbasp -n

Erase Chip:

avrdude -p t13 -c usbasp -u -e

Now that the chip is empty it is ready to be flashed with fresh firmware. Change the “current directory” by executing cd C:\avrusb so that AVRdude can find the file.

blf no strobe simple modes.

Flash Command:

avrdude -p t13 -c usbasp -u -Uflash:w:blf_a6.hex:a -Ulfuse:w:0x75:m -Uhfuse:w:0xfd:m